import requests
import click
import os
import logging
from typing import Union


VERSION = '1.0.0'
PathLike = Union[str, os.PathLike]


def download_emoji(emoji_name: str,
                   emoji_id: str,
                   folder_path: PathLike,
                   force: bool = False) -> None:
    URL = f'https://static-cdn.jtvnw.net/emoticons/v1/{emoji_id}/3.0'
    filepath = os.path.join(folder_path, emoji_name + '.png')

    if not os.path.isfile(filepath) or force:
        download_image(URL, filepath)
    else:
        logging.info(f'A file already exists at {filepath}')


def download_image(URL: str, filepath: PathLike) -> None:
    image = requests.get(URL)

    if image.headers.get('Content-Type') == 'image/png' and image.status_code == 200:
        image_file = image.content
        with open(filepath, 'wb') as f:
            f.write(image_file)
    else:
        logging.warning(f'Failed to download {URL}.')


def download_emojis_from_channel_id(channel_id: str, force: bool = False) -> None:
    api = requests.get(f'https://api.twitchemotes.com/api/v4/channels/{channel_id}').json()

    if api.get('display_name') and api.get('emotes'):
        folder_path = os.path.join(os.getcwd(), 'Emojis', api['display_name'])
        os.makedirs(folder_path, exist_ok=True)

        with click.progressbar(api['emotes'],
                               label=f'Downloading: {api["display_name"]}',
                               show_eta=False,
                               item_show_func=lambda emoji: emoji['code'] + '.png'
                               if emoji is not None else '') as emotes:
            for emoji in emotes:
                emoji_name, emoji_id = emoji['code'], emoji['id']
                download_emoji(emoji_name, emoji_id, folder_path, force=force)
    else:
        print('Not a valid Twitch Channel ID.')


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.argument('twitch_channel_id', type=click.INT)
@click.option('-f', '--force', is_flag=True, help="Forcefully download image files")
@click.option('-v', '--verbose', count=True, help="Show debug output")
@click.version_option(VERSION, '--version')
def main(twitch_channel_id: int, force: bool, verbose: int):
    """
    Twitch Emotes Batch Downloder
    """
    if verbose == 0:
        logging.basicConfig(level=logging.WARNING)
    if verbose == 1:
        logging.basicConfig(level=logging.INFO)
    elif verbose > 1:
        logging.basicConfig(level=logging.DEBUG)

    download_emojis_from_channel_id(str(twitch_channel_id), force)


if __name__ == '__main__':
    main()
