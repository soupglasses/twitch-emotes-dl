# Twitch Emotes DL
```
$ twitch-emotes-dl.py --help
Usage: twitch-emotes-dl.py [OPTIONS] TWITCH_CHANNEL_ID

  Twitch Emotes Batch Downloder

Options:
  -f, --force    Forcefully download image files
  -v, --verbose  Show debug output
  --version      Show the version and exit.
  -h, --help     Show this message and exit.
$ py twitch-emotes-dl.py 83118580
Downloading: Neko  [#########---------------------------]   26%  nkoPat.png
```
Twitch emotes download utility

## Usage

Uses https://twitchemotes.com/ API to find twitch emotes.

ID needed is found in the URL of twitchemotes after the `channels/`
* Example: https://twitchemotes.com/channels/83118580

## Install

Requirues `python3` and `pip` to be installed.

```
git clone https://github.com/imsofi/twitch-emotes-dl/ && cd twitch-emotes-dl
pip install -r requirements.txt
python twitch-emotes-dl.py --help
```

## Wishes

* Search functionality
  * Need to find a way to search for IDs
* Better Error handling of induvidual downloads.
* Better installation method
* Customizable download location
